from imbox import Imbox
import os
import sys
import logging


try:
    imap_host = os.environ['PAPERLESS_IMAP_HOST']
except KeyError:
    logging.error("Please set PAPERLESS_IMAP_HOST")
    sys.exit(1)

try:
    imap_user = os.environ['PAPERLESS_IMAP_USER']
except KeyError:
    logging.error("Please set PAPERLESS_IMAP_USER")
    sys.exit(1)

try:
    imap_pass = os.environ['PAPERLESS_IMAP_PASS']
except KeyError:
    logging.error("Please set PAPERLESS_IMAP_PASS")
    sys.exit(1)

consume_path = os.environ.get('PAPERLESS_CONSUME_PATH', '/consume')


def download_attachments():
    class InfoFilter(logging.Filter):
        def filter(self, rec):
            return rec.levelno in (logging.DEBUG, logging.INFO)

    std_out_stream_handler = logging.StreamHandler(sys.stdout)
    std_out_stream_handler.setLevel(logging.DEBUG)
    std_out_stream_handler.addFilter(InfoFilter())
    std_out_stream_handler.setFormatter(
            logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

    std_err_stream_handler = logging.StreamHandler(sys.stderr)
    std_err_stream_handler.setLevel(logging.WARNING)
    std_err_stream_handler.setFormatter(
            logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    root_logger.addHandler(std_out_stream_handler)
    root_logger.addHandler(std_err_stream_handler)

    if sys.version_info[0] < 3:
        logging.error(
                "Requires Python 3+, you are running version: %s",
                sys.version)
        exit(1)

    mail = Imbox(imap_host, username=imap_user, password=imap_pass)
    messages = mail.messages()
    for (uid, message) in messages:
        logging.info("Message '%s'", uid)
        for idx, attachment in enumerate(message.attachments):
            try:
                download_filename = attachment.get('filename')
                download_path = os.path.join(consume_path, download_filename)
                os.makedirs(os.path.dirname(os.path.abspath(download_path)),
                            exist_ok=True)
                logging.info("Downloading attachment '%s' to path %s",
                             attachment.get('filename'),
                             download_path)

                if os.path.isfile(download_path):
                    logging.warning("Overwriting file: '%s'", download_path)

                if os.path.isfile(download_path):
                    logging.warning("Overwriting file: '%s'", download_path)

                with open(download_path, "wb") as fp:
                    fp.write(attachment.get('content').read())

            except Exception as e:
                logging.exception(e)
                logging.error('Error saving file. Continuing...')
            else:
                mail.delete(uid)


if __name__ == '__main__':
    download_attachments()
